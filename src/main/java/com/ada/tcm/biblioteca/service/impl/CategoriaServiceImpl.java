package com.ada.tcm.biblioteca.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ada.tcm.biblioteca.mapper.CategoriaMapper;
import com.ada.tcm.biblioteca.model.dto.CategoriaDTO;
import com.ada.tcm.biblioteca.model.entity.Categoria;
import com.ada.tcm.biblioteca.repository.CategoriaRepository;
import com.ada.tcm.biblioteca.service.CategoriaService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class CategoriaServiceImpl implements CategoriaService {

  @Autowired
  private CategoriaRepository repository;

  @Autowired
  private CategoriaMapper mapper;

  @Override
  public List<CategoriaDTO> buscarTodos() {

    return mapper.parseListDTO(repository.findAll());
  }

  public CategoriaDTO buscarUm(Long id) {
		Optional<Categoria> categoriaOp = repository.findById(id);
		if(categoriaOp.isPresent()) {
			Categoria categoria = categoriaOp.get();
			return mapper.parseDTO(categoria);
		}
		
		throw new EntityNotFoundException();
	}

  public CategoriaDTO criar(CategoriaDTO categoriaDTO) {
		Categoria categoria = mapper.parseEntity(categoriaDTO);
		categoria.setId(null);
		repository.save(categoria);
		return mapper.parseDTO(categoria);
	}

  public CategoriaDTO editar(Long id, CategoriaDTO categoriaDTO) {
		
		if(repository.existsById(id)) {
			Categoria categoria = mapper.parseEntity(categoriaDTO);
			categoria.setId(id);
			categoria = repository.save(categoria);
			return mapper.parseDTO(categoria);
		}
		
		throw new EntityNotFoundException();
	}

  public void excluir(Long id) {
		if(!repository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		
		repository.deleteById(id);
	}
  
}
