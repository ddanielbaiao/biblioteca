package com.ada.tcm.biblioteca.service;

import java.util.List;

public interface BaseService<T>  {

  List<T> buscarTodos();
  T buscarUm(Long id);
  T criar(T entidade);
  T editar(Long id, T entidade);
  void excluir(Long id);
  
}
