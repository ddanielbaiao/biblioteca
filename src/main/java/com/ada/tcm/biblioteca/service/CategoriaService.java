package com.ada.tcm.biblioteca.service;

import com.ada.tcm.biblioteca.model.dto.CategoriaDTO;

public interface CategoriaService extends BaseService<CategoriaDTO> {}
