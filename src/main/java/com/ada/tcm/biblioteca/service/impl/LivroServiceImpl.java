package com.ada.tcm.biblioteca.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ada.tcm.biblioteca.mapper.LivroMapper;
import com.ada.tcm.biblioteca.model.dto.LivroDTO;
import com.ada.tcm.biblioteca.model.entity.Livro;
import com.ada.tcm.biblioteca.repository.LivroRepository;
import com.ada.tcm.biblioteca.service.LivroService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class LivroServiceImpl implements LivroService {

  @Autowired
  private LivroRepository repository;

  @Autowired
  private LivroMapper mapper;

  @Override
  public List<LivroDTO> buscarTodos() {

    return mapper.parseListDTO(repository.findAll());
  }

  public LivroDTO buscarUm(Long id) {
		Optional<Livro> livroOp = repository.findById(id);
		if(livroOp.isPresent()) {
			Livro livro = livroOp.get();
			return mapper.parseDTO(livro);
		}
		
		throw new EntityNotFoundException();
	}

  public LivroDTO criar(LivroDTO livroDTO) {
		Livro livro = mapper.parseEntity(livroDTO);
		livro.setId(null);
		repository.save(livro);
		return mapper.parseDTO(livro);
	}

  public LivroDTO editar(Long id, LivroDTO livroDTO) {
		
		if(repository.existsById(id)) {
			Livro livro = mapper.parseEntity(livroDTO);
			livro.setId(id);
			livro = repository.save(livro);
			return mapper.parseDTO(livro);
		}
		
		throw new EntityNotFoundException();
	}

  public void excluir(Long id) {
		if(!repository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		
		repository.deleteById(id);
	}
  
}
