package com.ada.tcm.biblioteca.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ada.tcm.biblioteca.mapper.EditoraMapper;
import com.ada.tcm.biblioteca.model.dto.EditoraDTO;
import com.ada.tcm.biblioteca.model.entity.Editora;
import com.ada.tcm.biblioteca.repository.EditoraRepository;
import com.ada.tcm.biblioteca.service.EditoraService;

import jakarta.persistence.EntityNotFoundException;

@Service
public class EditoraServiceImpl implements EditoraService {

  @Autowired
  private EditoraRepository repository;

  @Autowired
  private EditoraMapper mapper;

  @Override
  public List<EditoraDTO> buscarTodos() {

    return mapper.parseListDTO(repository.findAll());
  }

  public EditoraDTO buscarUm(Long id) {
		Optional<Editora> editoraOp = repository.findById(id);
		if(editoraOp.isPresent()) {
			Editora editora = editoraOp.get();
			return mapper.parseDTO(editora);
		}
		
		throw new EntityNotFoundException();
	}

  public EditoraDTO criar(EditoraDTO editoraDTO) {
		Editora editora = mapper.parseEntity(editoraDTO);
		editora.setId(null);
		repository.save(editora);
		return mapper.parseDTO(editora);
	}

  public EditoraDTO editar(Long id, EditoraDTO editoraDTO) {
		
		if(repository.existsById(id)) {
			Editora editora = mapper.parseEntity(editoraDTO);
			editora.setId(id);
			editora = repository.save(editora);
			return mapper.parseDTO(editora);
		}
		
		throw new EntityNotFoundException();
	}

  public void excluir(Long id) {
		if(!repository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		
		repository.deleteById(id);
	}
  
}
