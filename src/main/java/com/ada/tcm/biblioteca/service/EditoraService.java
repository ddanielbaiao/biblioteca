package com.ada.tcm.biblioteca.service;

import com.ada.tcm.biblioteca.model.dto.EditoraDTO;

public interface EditoraService extends BaseService<EditoraDTO> {}
