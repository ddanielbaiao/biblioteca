package com.ada.tcm.biblioteca.service;

import com.ada.tcm.biblioteca.model.dto.LivroDTO;

public interface LivroService extends BaseService<LivroDTO> {}
