package com.ada.tcm.biblioteca.model.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LivroDTO {

  private Long id;

  @NotBlank(message="O campo nome é obrigatório")
  private String nome;

  @NotBlank(message="O campo isbn é obrigatório")
  private String isbn;
  
  @NotBlank(message="O campo editoraId é obrigatório")
  private Long editoraId;

  @NotBlank(message="O campo categoriaId é obrigatório")
  private Long categoriaId;
  
}
