package com.ada.tcm.biblioteca.model.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EditoraDTO {

  private Long id;

  @NotBlank(message="O campo nome é obrigatório")
  private String nome;

  private String descricao;
  
}
