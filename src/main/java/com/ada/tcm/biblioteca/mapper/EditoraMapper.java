package com.ada.tcm.biblioteca.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.ada.tcm.biblioteca.model.dto.EditoraDTO;
import com.ada.tcm.biblioteca.model.entity.Editora;

@Mapper(componentModel = "spring")
public interface EditoraMapper {

  public List<EditoraDTO> parseListDTO(List<Editora> editoras);
  public List<Editora> parseListEntity(List<EditoraDTO> editoras);
  public EditoraDTO parseDTO(Editora editora);
  public Editora parseEntity(EditoraDTO editoraDTO);

}
