package com.ada.tcm.biblioteca.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.ada.tcm.biblioteca.model.dto.CategoriaDTO;
import com.ada.tcm.biblioteca.model.entity.Categoria;

@Mapper(componentModel = "spring")
public interface CategoriaMapper {

  public List<CategoriaDTO> parseListDTO(List<Categoria> categorias);
  public List<Categoria> parseListEntity(List<CategoriaDTO> categoriasDTO);
  public CategoriaDTO parseDTO(Categoria categoria);
  public Categoria parseEntity(CategoriaDTO categoriaDTO);

}
