package com.ada.tcm.biblioteca.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.ada.tcm.biblioteca.model.dto.LivroDTO;
import com.ada.tcm.biblioteca.model.entity.Livro;

@Mapper(componentModel = "spring")
public interface LivroMapper {

  public List<LivroDTO> parseListDTO(List<Livro> livros);
  public List<Livro> parseListEntity(List<LivroDTO> livrosDTO);
  public LivroDTO parseDTO(Livro livro);
  public Livro parseEntity(LivroDTO livroDTO);

}
