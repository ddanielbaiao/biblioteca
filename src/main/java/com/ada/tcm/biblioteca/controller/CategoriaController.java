package com.ada.tcm.biblioteca.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ada.tcm.biblioteca.model.dto.CategoriaDTO;
import com.ada.tcm.biblioteca.service.CategoriaService;

@RestController
@RequestMapping("/categorias")
public class CategoriaController extends BaseController<CategoriaDTO, CategoriaService> {}
