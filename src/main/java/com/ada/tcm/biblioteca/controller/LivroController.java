package com.ada.tcm.biblioteca.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ada.tcm.biblioteca.model.dto.LivroDTO;
import com.ada.tcm.biblioteca.service.LivroService;

@RestController
@RequestMapping("/livros")
public class LivroController extends BaseController<LivroDTO, LivroService> {}