package com.ada.tcm.biblioteca.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ada.tcm.biblioteca.model.dto.EditoraDTO;
import com.ada.tcm.biblioteca.service.EditoraService;

@RestController
@RequestMapping("/editoras")
public class EditoraController extends BaseController<EditoraDTO, EditoraService> {}