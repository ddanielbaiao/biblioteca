package com.ada.tcm.biblioteca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ada.tcm.biblioteca.model.entity.Editora;

@Repository
public interface EditoraRepository extends JpaRepository<Editora, Long> {
  
}